/* SerialEcho
   Demo sketch that simply writes received serial data to the display.
   Works best for code that is blatting a line of text at a time.
   * No line wrapping is done, more than 16 characters just disappears.
   * Newlines are rendered as four horizontal lines (like a hamburger icon)

   Library dependencies:
   * new-liquidcrystal: https://bitbucket.org/fmalpartida/new-liquidcrystal

   Thanks to John Boxall for a lovely tutorial documenting this display.
   http://tronixstuff.com/2014/09/24/tutorial-serial-pcf8574-backpacks-hd44780-compatible-lcd-modules-arduino/

   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <Wire.h>

#include <LCD.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);

void setup() {
  Serial.begin(115200);
  lcd.begin(16, 2);
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);

  lcd.home();
  lcd.print("SerialEcho ready");
  Serial.println("SerialEcho ready");
}

void loop() {
  // when characters arrive over the serial port...
  if (Serial.available()) {
    // wait a bit for the entire message to arrive
    delay(100);
    // clear the screen
    lcd.clear();
    // read all the available characters
    while (Serial.available() > 0) {
      // display each character to the LCD
      lcd.write(Serial.read());
    }
  }
}
