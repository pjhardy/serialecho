# SHPXXXVI-19 Arduino sketches

Two test sketches currently exist, designed to receive serial data and provide
feedback on an attached I2C LCD display.

* SerialEcho is a very basic echo client that writes received text to the
  screen.
* SerialParser is a barebones implementation of the current bucket command
  set. Known commands are parsed and written to the screen. Unknown commands
  receive a generic error. Serial responses are what the prod implementation
  will generate.

## Building and installing these sketches

* Install the latest version of the Arduino IDE from https://www.arduino.cc/
* Install the new-liquidcrystal library from
  https://bitbucket.org/fmalpartida/new-liquidcrystal
* Install the SerialCommand library from
  https://github.com/kroimon/Arduino-SerialCommand
* Ensure the `arduino` command is in your path. Something like this will
  work on OS X: `ln -s /Applications/Arduino.app/Contents/MacOS/Arduino /usr/local/bin/arduino`
* Determine which port the arduino is plugged in to. On OS X this will be
  something like `/dev/tty.usbmodem1421` where the number varies depending
  on where in the USB device tree your arduino is.
* cd in to the appropriate directory and run
  `ARDUINO_PORT=/dev/tty.usbmodem1421 make install`, substituting your
  actual serial port in ARDUINO_PORT.

## Usage notes

These sketches work best when sending whole lines at once. Commands must
be separated by a newline character.

The easiest way to interact with these sketches manually is to use the
Serial Monitor in the Arduino IDE.

* Start the Arduino IDE.
* Go to Tools -> Port and select the appropriate serial port.
* Go to Tools -> Serial Monitor.
* Ensure the speed in the serial monitor is set to 115200.
* Ensure the line ending setting next to the port speed is set to Newline.
* Type in the text field at the top, and hit enter to send.

