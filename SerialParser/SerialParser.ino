/* SerialParser
   Expands on SerialEcho by partially implementing the bucket command
   set. Known commands generate success text on screen. Unknown commands
   produce a generic error. Serial responses are as per documented
   bucket command set.

   Library dependencies:
   * new-liquidcrystal: https://bitbucket.org/fmalpartida/new-liquidcrystal
   * SerialCommand: https://github.com/kroimon/Arduino-SerialCommand

   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <Wire.h>

#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <SerialCommand.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7);
SerialCommand sCmd;

void setup() {
  Serial.begin(115200);
  lcdInit();
  serialInit();

  lcd.home();
  lcd.print("SerialParser");
  lcd.setCursor(0, 1);
  lcd.print("ready...");

  Serial.println("SerialParser ready...");
}

void loop() {
  sCmd.readSerial();
}

void lcdInit() {
  lcd.begin(16, 2);
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);
}

void serialInit() {
  sCmd.addCommand("spin", spinCmd);
  sCmd.addCommand("throb", throbCmd);
  sCmd.addCommand("percent", percentCmd);
  sCmd.addCommand("percentgrad", percentgradCmd);
  sCmd.addCommand("kitt", kittCmd);
  sCmd.addCommand("demo", demoCmd);
  sCmd.addCommand("help", helpCmd);
  sCmd.setDefaultHandler(unknownCmd);
}

void spinCmd() {
  lcd.clear();
  lcd.print("spin command");
  lcd.setCursor(0, 1);
  printArgs(2);
  Serial.println("OK");
}

void throbCmd() {
  lcd.clear();
  lcd.print("throb command");
  lcd.setCursor(0, 1);
  printArgs(2);
  Serial.println("OK");
}

void percentCmd() {
  lcd.clear();
  lcd.print("percent command");
  lcd.setCursor(0, 1);
  printArgs(2);
  Serial.println("OK");
}

void percentgradCmd() {
  lcd.clear();
  lcd.print("percentgrad command");
  lcd.setCursor(0, 1);
  printArgs(2);
  Serial.println("OK");
}

void kittCmd() {
  lcd.clear();
  lcd.print("kitt command");
  lcd.setCursor(0, 1);
  printArgs(1);
  Serial.println("OK");
}

void demoCmd() {
  lcd.clear();
  lcd.print("kitt command");
  lcd.setCursor(0, 1);
  lcd.print("no args");
  Serial.println("OK");
}

void helpCmd() {
  lcd.clear();
  lcd.print("help command");
  lcd.setCursor(0, 1);
  lcd.print("no args");
  Serial.print(F("SerialParser\n============\nSend a command with parameters separated by spaces.\nRefer to EAC for full documentation on available commands.\n"));
}

void unknownCmd() {
  lcd.clear();
  lcd.print("Invalid command");
  Serial.println("error");
}

void printArgs(int numArgs) {
  for (int i=0; i<numArgs; i++) {
    lcd.print(sCmd.next());
    lcd.print(" ");
  }
}